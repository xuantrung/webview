//
//  ViewController.swift
//  test_website
//
//  Created by Admin on 3/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
    
    var webView : WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let configuration = WKWebViewConfiguration()
        configuration.allowsInlineMediaPlayback = false // ngăn mặc định fullscreen khi chạy video
        configuration.mediaTypesRequiringUserActionForPlayback = []
        
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), configuration: configuration)
        self.view.addSubview(webView)
        webView.navigationDelegate = self
        requestURL()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // listen for videos playing in fullscreen
        NotificationCenter.default.addObserver(self, selector: #selector(onDidEnterFullscreen(_:)), name: UIWindow.didBecomeVisibleNotification, object: nil)
        
        // listen for videos stopping to play in fullscreen
        NotificationCenter.default.addObserver(self, selector: #selector(onDidLeaveFullscreen(_:)), name: UIWindow.didBecomeHiddenNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     
        
        // remove video listeners
        NotificationCenter.default.removeObserver(self, name: UIWindow.didBecomeVisibleNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIWindow.didBecomeHiddenNotification, object: nil)
    }
    
    @objc func onDidEnterFullscreen(_ notification: Notification) {
        print("video is now playing in fullscreen")
        
    }
    
    @objc func onDidLeaveFullscreen(_ notification: Notification) {
        print("video has stopped playing in fullscreen")
    }

    func requestURL(){
        let url = URL(string: "https://mobile.twitter.com/login")!
        let urlRequest = URLRequest(url: url)
        self.webView.load(urlRequest)
    }
    
    

}

